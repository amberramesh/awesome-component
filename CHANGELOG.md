## [1.0.1](http://bitbucket.org/amberramesh/awesome-component/compare/v1.0.0...v1.0.1) (2020-12-14)


### Bug Fixes

* [skip ci] merge branch ([1ae38c8](http://bitbucket.org/amberramesh/awesome-component/commits/1ae38c8c1b601d0135d2085776c255d213ad85b6))
* **component:** update name and inner text ([2496c70](http://bitbucket.org/amberramesh/awesome-component/commits/2496c70e1c50a2f92e87c1b5b268e314cbc77200))

# 1.0.0-alpha.1 (2020-12-14)


### Bug Fixes

* **component:** update name and inner text ([2496c70](http://bitbucket.org/amberramesh/awesome-component/commits/2496c70e1c50a2f92e87c1b5b268e314cbc77200))


### Features

* **plugin:** add publish-utils ([555eea2](http://bitbucket.org/amberramesh/awesome-component/commits/555eea2fadbc00e160fcc786c5d2c8b0a8b694ed))

import replace from '@rollup/plugin-replace';
import alias from '@rollup/plugin-alias';
import globals from 'rollup-plugin-node-globals';
import builtins from 'rollup-plugin-node-builtins';
import babel from '@rollup/plugin-babel';
import vue from 'rollup-plugin-vue';
import postcss from 'rollup-plugin-postcss';
import smartAsset from 'rollup-plugin-smart-asset';
import json from '@rollup/plugin-json';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';

export default {
  input: 'src/index.js',
  output: {
    file: 'lib/index.esm.js',
    format: 'esm',
    sourcemap: true,
    exports: 'named',
  },
  plugins: [
    replace({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    alias({
      entries: {
        vue: 'vue/dist/vue.runtime.esm.js',
        vuex: 'vuex/dist/vuex.esm.js',
      },
    }),
    babel({
      exclude: 'node_modules/**',
      babelHelpers: 'runtime',
    }),
    vue(),
    postcss({
      minimize: true,
      sourceMap: true,
    }),
    smartAsset({
      include: ['**/*.svg', '**/*.png', '**/*.jp(e)?g', '**/*.gif', '**/*.webp', '**/*.woff', '**/*.woff2'],
      url: 'copy',
      assetsPath: 'assets',
      nameFormat: '[name]__[hash][ext]',
      keepImport: true,
    }),
    json({
      compact: true,
      preferConst: true,
    }),
    resolve({
      browser: true,
      extensions: ['.js', '.vue'],
    }),
    commonjs({
      include: 'node_modules/**',
      ignoreGlobal: true,
    }),
    globals(),
    builtins(),
  ],
  external: ['vue', 'vuex'],
};
